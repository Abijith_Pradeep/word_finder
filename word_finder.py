#Library to handle json files
import json  
import random 
#Load the json file (dictionary file)
data = json.load(open("data.json"))
#Receiving the jumbled letters
s = input("Enter the letters : ").lower()
n=int(input("Enter the number of letters in the word : "))
l=int(input("Enter the number of words you need : "))
#A list that stores all the words that have already been checked
words=[]
i=0

def main():
    global i
    while(i<l):
        word=''.join(random.sample(s,n))
        if word not in words :
            if word in data :
                print(word)
                i = i + 1
            words.append(word)
            
main()



