# Word Finder 

A solution for all word finding games.

User can input the jumbled letters and the number of letters you need for the word. And the program will display all the meaningful words.


## Working

Programs shuffles the inputed letters to form multiple words. These words are then cross checked with the dictionary file. If the shuffled word is present in the dictionary file, then the word is printed. This process is continued until the number of words as mentioned by the users are found.
